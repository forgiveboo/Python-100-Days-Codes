#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
百分制成绩转换为等级制成绩
要求：
    1. 如果输入的成绩在90分以上(含90分)输出A
    2. 如果输入的成绩在80分-90分(不含90分)输出B
    1. 如果输入的成绩在70分-80分(不含80分)输出C
    1. 如果输入的成绩在60分-70分(不含70分)输出D
    1. 如果输入的成绩在60分以下(含90分)输出E 
"""
score = float(input('请输入成绩：'))
if score >= 90:
    print('成绩为A')
elif score > 80 and score < 90:
    print('成绩为B')
elif score > 70 and score < 80:
    print('成绩为C')
elif score > 60 and score < 70:
    print('成绩为D')
else:
    print('成绩为E')

######################
#这里可以用变量来保存成绩对应的的等级
#这样就不用写那么多个print()了
######################
