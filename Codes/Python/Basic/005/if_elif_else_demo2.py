#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
输入三条边长，如果能组成三角形就计算周长和面积
要组成三角形必须要任意两边之和大于第三边
"""
a = float(input('a = '))
b = float(input('b = '))
c = float(input('c = '))
if a + b > c and a + c > b and b + c > a:
    circle = a + b + c
    print('圆的周长为：%f' % circle)
    half = circle / 2
    area = (half * (half - a) * (half - b) * (half - c)) ** 0.5  # 海伦公式
    print(f'圆的面积为：{area}')
else:
    print('不能构成三角形')
