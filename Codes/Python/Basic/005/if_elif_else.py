#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
利用if语句求分段函数的值
     = 3x - 5 (x > 1)
f(x) = x + 2  (-1 <= x <= 1)
     = 5x + 3 (x < -1)
"""
# 分支结构非嵌套实现
num = 0
x = int(input('请输入x的值：'))
if x > 1:
    num = 3 * x - 5
elif x >= -1 and x <= 1:
    num = x + 2
else:
    num = 5 * x + 3
print('f(x) = %d' % num)

# 分支结构嵌套实现
num1 = 0
x1 = int(input('请输入x1的值：'))
if x > 1:
    num1 = 3 * x1 - 5
else:
    if x1 >= -1 and x1 <= 1:
        num1 = x1 + 2
    else:
        num1 = 5 * x1 + 3
print('f(x) = %d' % num1)

##########################################
#如非必要尽量不要用嵌套，会影响代码可读性#
##########################################
