#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
对1-100的偶数求和
"""
total = 0
for x in range(2, 101, 2):  # 随机数从2开始取，每次取的时候加2
    total += x
print(total)
