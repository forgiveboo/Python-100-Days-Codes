#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
判断是不是润年
如果是则输出是润年
如果不是则输出不是润年
"""
year = int(input('请输入年份：'))
is_leap = year % 4 == 0 and year % 100 != 0 or year % 400 == 0
print(is_leap)

##################
#这个例子有待调整#
##################
