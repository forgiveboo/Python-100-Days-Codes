#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
算数运算符
算数运算符返回的都是数值
"""
print(321 + 123)    # 加法运算
print(321 - 123)    # 减法运算
print(321 * 123)    # 乘法运算
print(321 / 123)    # 除法运算
print(321 % 123)    # 求模运算(取余运算)
print(321 // 123)   # 整除运算(取整运算))
print(321 ** 123)   # 求幂运算


"""
赋值运算符和复合赋值运算符
赋值运算符可以和任何算副运算符复合
赋值运算符返回的都是数值
"""
a = 10
b = 3
a += b      # a = a + b
a -= b      # a = a - b
a *= a + 2  # a = a * (a + 2)
a /= b      # a = a / b
print(a)


"""
比较运算符和逻辑运算符
比较运算符和逻辑运算符返回的都是布尔型
比较运算符的优先级高于赋值运算符
"""
############################################
#先计算右边比较运算符的值再赋值给左边的变量#
############################################
flag0 = 1 == 1              # True
flag1 = 3 > 2               # True
flag2 = 2 < 1               # False
flag3 = 1 != 2              # True
flag4 = flag0 and flag1     # True     
flag5 = flag1 or flag2      # True
flag6 = not flag4           # False

print('flag0 = ', flag0)      
print('flag1 = ', flag1)      
print('flag2 = ', flag2)      
print('flag3 = ', flag3)      
print('flag4 = ', flag4)      
print('flag5 = ', flag5)      
print('flag6 = ', flag6)      
