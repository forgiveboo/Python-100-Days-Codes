#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
输入半径计算圆的周长和面积
"""
r = float(input('请输入圆的半径：'))
c = 2 * 3.1415 * r  # 周长
s = 3.1415 * r * r  # 面积
print('圆的周长为：%.2f' % c)
print('圆的面积为：%.2f' % s)
