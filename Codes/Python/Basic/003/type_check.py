#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
使用type()检查变量的类型
"""

a = 100
b = 12.89
c = 'Hello, world'
d = True

print(type(a))  # <class 'int'>
print(type(b))  # <class 'float'>
print(type(c))  # <class 'str'>
print(type(d))  # <class 'boll'>

