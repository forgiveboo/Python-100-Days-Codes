#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Python中的类型转换
"""

a = 100             # 整数
b = 12.5            # 浮点数
c = 'Hello, world'  # 字符串
d = True            # 布尔型
e = None            # None

# 整数转换为浮点数
print(float(a))   # 100,0 
# 浮点数转换为字符串
print(str(b))     # 12.5  
# 非字符串转换为布尔型
print(bool(c))    # True(如果不是空字符串就会返回True)  
# 空字符串转换为布尔型
print(bool(e))    # False(如果是空字符串则返回False)
# 布尔型转换为整形
print(int(d))     # 1(布尔值为True则返回1,布尔值为False则返回0)
# 将整数转换为字符
print(chr(a))     # 刚好对应字母表的d
# 将字符转换成整数
print(ord('a'))   # 97(Python中字符和字符串表示法相同)

# 将字符串转换为整数
# print(int(c))     # 不能将字符串转换为整数

"""
1. 在Python中，可以使用变量来保存数据，也可以通过内置函数来进行数据转换
2. 变量可以参加运算
3. 函数ord()是将字符转换为相应的编码
4. Python中，只有7种数据转换是合法的，如上述代码的前七种数据转换
"""
